DROP TABLE IF EXISTS wae1_users;
DROP TABLE IF EXISTS wae1_recipes;
DROP TABLE IF EXISTS wae1_comments;
DROP TABLE IF EXISTS wae1_images;

CREATE TABLE IF NOT EXISTS wae1_users(
	id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
	username VARCHAR(30) NOT NULL UNIQUE,
	password_hash CHAR(64) NOT NULL,
	email VARCHAR(50),
	role ENUM('admin', 'expert', 'competent', 'novice') NOT NULL,
	updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
                ON UPDATE CURRENT_TIMESTAMP,
	created_at TIMESTAMP,
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS wae1_recipes (
	id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
	title VARCHAR(100) NOT NULL,
	ingredients TEXT,
	description TEXT,
	profession INTEGER,
	duration INTEGER,
	persons INTEGER,
	rating_sum INTEGER,
	rating_count INTEGER,
	nutrient_value INTEGER,
	user_id INTEGER UNSIGNED NOT NULL REFERENCES wae1_users(id),
	updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
                ON UPDATE CURRENT_TIMESTAMP,
	created_at TIMESTAMP,
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS wae1_comments (
	id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
	name VARCHAR(30) NOT NULL,
	comment TEXT NOT NULL,
	recipe_id INTEGER NOT NULL 	REFERENCES wae1_recipes(id),
	updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
                ON UPDATE CURRENT_TIMESTAMP,
	created_at TIMESTAMP,
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS wae1_images (
	id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
	link TEXT NOT NULL,
	recipe INTEGER NOT NULL REFERENCES wae1_recipes(id),
	updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
                ON UPDATE CURRENT_TIMESTAMP,
	created_at TIMESTAMP,
	PRIMARY KEY (id)
);

INSERT INTO wae1_users (username,password_hash,email,role,created_at) VALUES
('andreas','71c6608bbce71b12712a19330e5f8914905e0af0d14385d3a9d879d4d35223b1','andreas.ofner@gmx.at','admin',NULL),
('raluca','71c6608bbce71b12712a19330e5f8914905e0af0d14385d3a9d879d4d35223b1','ralu.daiconu93@yahoo.com','admin',NULL);