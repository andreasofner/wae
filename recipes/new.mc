<h1 class="titluReteta">Neues Rezept anlegen</h1>
<div class="">
	<& _form_recipe.mi, method => "create", recipe => {} &>
</div>

<%init>
if ( $m->req->session->{user_id} < 1 ) {
	$m->req->session->{message_error} = "Sie müssen eingeloggt sein, um Rezepte erstellen zu können!";
	$m->redirect( "/wae01/login");
}
</%init>