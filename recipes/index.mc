<div class="culinar">
<h1 class="titlu3"><i>Rezepte</i></h1>
<div class="rete">
% foreach my $recipe ( @recipes ) {
	<span><& _recipe_short.mi, recipe => $recipe &></span>
% }
</div>
<%init>
use Data::Dumper;

my @recipes = ();

my $dbh = Ws14::DBI->dbh();
my $sth = $dbh->prepare("SELECT * FROM wae1_recipes");
$sth->execute();

while (my $res = $sth->fetchrow_hashref()) {
	push( @recipes, $res );
}
</%init>
</div>