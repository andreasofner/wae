<h1><% $recipe->{title} %></h1>

<div class="">
	<& $file_url, method => "update", recipe => $recipe &>
</div>

<%init>
my $session = $m->req->session;

my $dbh = Ws14::DBI->dbh();
my $sth = $dbh->prepare("SELECT * FROM wae1_recipes WHERE id=?");
$sth->execute($.id);

my $recipe = $sth->fetchrow_hashref();

if ($session->{user_id} ne $recipe->{user_id} && $.method eq "edit") {
	$session->{message_error} = "Sie müssen der Autor dieses Rezeptes sein, um es bearbeiten zu können.";
	$m->redirect("/wae01/recipes/$recipe->{id}");
}

my $file_url = "_recipe.mi";
if ($.method eq 'edit') {
	$file_url = "_form_recipe.mi";
}
</%init>
<br><br><a href="/wae01/recipes">Zurück zur Übersicht</a>

<%class>
route "{id:[0-9]+}";
route "{id:[0-9]+}/:method";
</%class>