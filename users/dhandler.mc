	<div class = "jumbotron">
		<img src="/wae01/static/images/chef.jpg" style="width:200px;height:200px" align="top" >
    	<center><h1><h1 class="numeUser">Welcome, Chef <% $$user{username} %></h1>
    </div>

<div class="">
	<& $file_url, method =>"update", user => $user &>
</div>

<%init>
use Data::Dumper;

# Get current session
my $session = $m->req->session;

if ( $.id ne $session->{user_id} && 'admin' ne $session->{user_role} ) {
	$m->redirect("/wae01/users");
}

my $file_url = "_user.mi";
if ($.method eq 'edit') {
	$file_url = "_form_user.mi";
}

my $dbh = Ws14::DBI->dbh();
my $sth = $dbh->prepare("SELECT * FROM wae1_users WHERE id=?");
$sth->execute($.id);

my $user = $sth->fetchrow_hashref();
</%init>

<br /><a href="/wae01/users">Zurück zur Übersicht</a>

<%class>
route "{id:[0-9]+}";
route "{id:[0-9]+}/:method";
</%class>