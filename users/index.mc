<h1>Benutzerliste</h1>

% 	foreach my $user (@users) {
		<& _user_short.mi, user=>$user &>
%	}

<%init>
use Data::Dumper;

my @users = ();

my $dbh = Ws14::DBI->dbh();
my $sth = $dbh->prepare("SELECT id, username, email FROM wae1_users");
$sth->execute();

while (my $res = $sth->fetchrow_hashref()) {
	push( @users, $res );
}
</%init>