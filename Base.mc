<%class>
has 'maintitle' => (default => 'WAE Group 1');
</%class>

<%augment wrap>
  <html>
    <head>
      <link rel="stylesheet" type="text/css" href="/static/css/style.css">
	  <link rel="stylesheet" href="/wae01/static/css/app.css">
      <script src="/static/js/ckeditor/ckeditor.js"></script>
% $.Defer {{
      <title><% $.maintitle %></title>
% }}
    </head>
    <body>
		<header>
			<& _navigation.mi &>
		</header>
    <div class="container">
      <% inner() %>
      <& _footer.mi, grp => '1' &>
    </div>
	   <& _debug.mi &>
    </body>
  </html>
</%augment>

<%flags>
extends => undef
</%flags>
