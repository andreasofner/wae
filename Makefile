all: deploy

deploy: copy
	ssh wae01@weng.culturall.com 'tar -xvzf wae01.tar.gz && rm -rf wae01.tar.gz ._* recipes/._*'

copy: gzip
	scp wae01.tar.gz wae01@weng.culturall.com:/usr/local/htdocs/ws14/comps/wae01

gzip: tar
	gzip wae01.tar

tar: clean
	tar --exclude='wiki' -cf wae01.tar *

unzip:
	tar -xvzf wae01.tar.gz

clean:
	rm -rf wae.tar wae01.tar.gz
